#!/bin/bash
#
#   /etc/rc.d/init.d/someprog
#
# Starts the mnemosyne daemon
#
# chkconfig: 345 80 20
# description: the mnemosyne daemon
# processname: mnemosyne
# config: /var/bot/mnemosyne/mnemosyne.cfg

# Source function library.
. /etc/rc.d/init.d/functions

cd /var/bot/mnemosyne

prog="mnemosyne"
exec="/usr/bin/python3 /var/bot/mnemosyne/mnemosyne.py"
[ -e /etc/sysconfig/$prog ] && . /etc/sysconfig/$prog
lockfile="/var/bot/$prog/$prog.lock"
pidfile="/var/bot/$prog/$prog.pid"
RETVAL=0

check() {
    [ `id -u` = 0 ] || exit 4
    test -x $exec || exit 5
}

start() {
    # check
    if [ ! -f $lockfile ]; then
	# echo "Starting Mnemosyne"
        echo -n $"Starting $prog: " 
        daemon --user=bots "nohup $exec" &
        RETVAL=$?
        if [ $RETVAL -eq 0 ]; then
          touch $lockfile
          ps aux | grep $exec | grep -v grep | tr -s " " | cut -d " " -f2 > $pidfile
        fi
        echo
    fi
    return $RETVAL
}

stop() {
    # check
    echo -n $"Stopping $prog: "
    killproc $exec && cat $pidfile | kill
    RETVAL=$?
    rm -f $lockfile
    rm -f $pidfile
    if [ $RETVAL -eq 0 ]; then
      success; echo
    else
      failure; echo
    fi
    echo
    return $RETVAL
}

restart() {
    stop
    start
}   

case "$1" in
start)
    start
    ;;
stop)
    stop
    ;;
restart)
    restart
    ;;
status)
    status $prog
    RETVAL=$?
    ;;
*)
    echo $"Usage: $0 {start|stop|restart|status}"
    RETVAL=2
esac

exit $RETVAL
