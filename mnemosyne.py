#!/usr/bin/env python3

import praw
import os
import sys
import configparser
import requests
import time
import datetime
import traceback
import re
import random
import pprint
import archiveis
from furl import furl
from time import sleep
from urllib.parse import urlparse


def main():
	print('Mnemosyne reborn by /u/ITSigno')

	print(os.getcwd())
	if not os.path.exists('mnemosyne.cfg'):
		print('No config file.')
		sys.exit()

	config = configparser.ConfigParser()
	config.read('mnemosyne.cfg')
	USERNAME = config.get('Bot', 'username')
	PASSWORD = config.get('Bot', 'password')
	USERAGENT = config.get('Bot', 'useragent')

	SUBREDDIT = config.get('Reddit', 'subreddit')
	REQ_LIMIT = config.getint('Reddit', 'request_limit')

	SUBMIT_URL = config.get('Config', 'submit_url')
	SLEEP_TIME = config.getint('Config', 'sleep_time')

	#EXCLUDE = config.get('Config', 'exclude')
	EXCLUDE = re.compile(r'youtube\.com|archive\.(ph|is|li|fo|today|vn|md)|web\.archive\.org|youtu\.be')
	REDDIT_REGEX = re.compile(r'(www|np)\.reddit\.com')

	r = praw.Reddit()
	pprint.pprint(r)
	#r.login(USERNAME,PASSWORD, disable_warning=True)

	s = r.subreddit(SUBREDDIT)
	pprint.pprint(s)

	submitid = ''

	d_head = "Archive links for this discussion: \n\n"
	p_head = "Archive links for this post: \n\n"

	foot = "----\n\nI am Mnemosyne reborn. {} ^^^/r/botsrights"

	if not os.path.exists('flavor-flav.txt'):
		flavor_text = []
	else:
		with open('flavor-flav.txt', "r") as f:
			flavor_text = f.read()
			flavor_text = flavor_text.split("\n")
			flavor_text = list(filter(None, flavor_text))

	# scraper = cfscrape.create_scraper()

	if not os.path.exists('replied_to.txt'):
		already_done = []
	else:
		with open('replied_to.txt', "r") as f:
			already_done = f.read()
			already_done = already_done.split("\n")
			alreay_done = list(filter(None, already_done))

	while True:
		try:
			# print 'new run'
			for p in s.new(limit=REQ_LIMIT):
				#if p.saved is False and p.locked is False and p.id not in already_done:
				if p.locked is False and p.id not in already_done:
					print("\n")
					# print('saving')
					already_done.append(p.id)
					# p.save()
					print(u'{} ({})'.format(p.title, p.id))
					with open('replied_to.txt', 'a') as f:
						f.write(p.id + "\n")

					submitid = get_submitid()

					# print("submitid: {}\n".format(submitid))

					parsed_uri = urlparse(p.url)

					# force usage of old reddit
					if REDDIT_REGEX.search(parsed_uri.netloc, 0) is not None:
						new_netloc = 'old.reddit.com'.join(parsed_uri.netloc.rsplit(parsed_uri.hostname, 1))
						parsed_uri = parsed_uri._replace(netloc=new_netloc)

					print("URL to be archived: {}".format(parsed_uri.geturl()))
					if EXCLUDE.search(parsed_uri.netloc, 0) is not None:
						continue
					print("Passed Exclude")
					url = get_archive_url(SUBMIT_URL,parsed_uri.geturl(), submitid)  # archiveis.capture(parsed_uri.geturl())
					# get_archive_url(SUBMIT_URL,parsed_uri.geturl(), submitid)
					if not url:
						print("Archive failed")
						log("Failed to archive: " + p.permalink + "\nurl: " + p.url + "\ndomain: " + parsed_uri.netloc, file="failed.txt")
						continue # failed to get url

					url = re.sub('/wip/', '/', url);
					print("Archive url: {}".format(url))

					head = d_head if p.is_self else p_head
	
					ft = random.choice(flavor_text)
					
					c = head + "* **Archive:** " + url + "\n\n" + foot.format(ft)
					# c = "Archiving currently broken. Please [archive](https://archive.ph) manually" + "\n\n" + foot.format(ft)
					# print(u"{}\n".format(c))
					#p.add_comment(c)
					p.reply(c)
					# print("replied\n")
					# sys.exit()
					sleep(10)
						
			sleep(SLEEP_TIME)
		except Exception as e:
			log(traceback.format_exc() + "\nERROR: " + str(e) + "\nlast url: " + str(parsed_uri) + "\n", file="error_log.txt")
			sleep(SLEEP_TIME)
			continue
			

def log(message, **kwargs):
	logfile = kwargs.get('file', 'error_log.txt')
	now = datetime.datetime.now()
	with open(logfile, 'a') as f:
		f.write(now.strftime("%m-%d-%Y %H:%M\n"))
		message = message + "\n"  # .encode('utf-8')
		f.write(message)


def get_submitid():
	r_headers = {'host': 'archive.ph', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 'cache-control' : 'max-age=0'}
	req = requests.get('https://archive.ph', headers=r_headers)
	resp = req.text
	submitid = re.findall(r'<input type="hidden" name="submitid" value="([^"]*)"/>', resp, flags=re.MULTILINE)
	if (submitid is not None) and hasattr(submitid, '__len__') and (not isinstance(submitid, str)) and len(submitid):
		submitid = submitid.pop()
		# print "submitid: {}\n".format(submitid)
		return submitid
	else:
		# print(submitid)
		log("Failed to get submitid")
		log(req.text, file="debug.log")
	return ''


def get_archive_url(service_url, url, submitid):
	# headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36', 'referer': 'https://archive.ph', 'connection': 'keep-alive', 'accept-language': 'en-US,en;q=0.8', 'cache-control':'max-age=0', 'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'accept-encoding': 'gzip, deflate, sdch, br'}
	#submitid = get_submitid()

	if not submitid:
		return ''

	r_headers = {'host': 'archive.ph', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 'referer': 'https://archive.ph'}
	req = requests.post(service_url, data={'url':url, 'submitid':submitid}, allow_redirects=False, timeout=20, headers=r_headers)
	loc = req.headers.get('Location')
	ref = req.headers.get('Refresh')

	# log("serivce url: " + str(service_url) + "\nloc: " + str(loc) + "\nref: " + str(ref) + "\n", file="testing.txt")

	# log("Status: " + str(req.status_code) + "\nResponse headers: " + pprint.pformat(req.headers) + "\nRequest headers: " + pprint.pformat(req.request.headers) + "\nservice: " + service_url + "\nurl: " + url + "\nloc: " + str(loc) + "\nref: " + str(ref), file="debug.log")

	archive_url = ''

	if ref:
		archive_url = re.sub(r'^\d+;url=','', ref, flags=re.IGNORECASE)

		if re.search(r'^\d+$', str(ref).strip()):
			if get_archive_url.tries < get_archive_url.max_tries:
				get_archive_url.tries += 1
				stime = min([8, int(ref)])
				sleep(stime)
				archive_url = get_archive_url(service_url, url, submitid)
			else:
				archive_url = ''

		if re.search(r'^(https?:)?//archive\.(fo|is|today|li|vn|md)/submit/?',archive_url):
			archive_url = ''
			# log("url: " + url + "\nloc: " + str(loc) + "\nref: " + str(ref) + "\ntext: " + str(req.text), file="failed.txt")

			if re.search(r'^/cdn-cgi/', archive_url) and not re.search('/cdn-cgi/', service_url):
				archive_url = get_archive_url("https://archive.ph"+str(archive_url), url)

		# if get_archive_url.tries < get_archive_url.max_tries and re.search('^\d+$', str(ref)):
		# get_archive_url.tries += 1
		# stime = min([8, int(ref)])
		# sleep(stime)
		# archive_url = get_archive_url(service_url, url, scraper)

	else:
		# failed to get fresh archive
		# fallback will use a hash if possible, query param otherwise

		# print "attempting to amend url and retry\n"

		f = furl(url)
		if not str(f.fragment):
			f.fragment.path = str(time.time());
		else:
			f.add({'ar': str(time.time())})

		# print f.url

		freq = requests.post(service_url, data={'url':f.url, 'submitid':submitid}, allow_redirects=False, timeout=20, headers=r_headers)
		fref = freq.headers.get('refresh')
		floc = freq.headers.get('location')

		# log(str(freq.status_code) + "\nservice: " + service_url + "\nfurl: " + f.url + "\nfloc: " + str(floc) + "\nfref: " + str(fref), file="debug.log")

		if fref:
			archive_url = re.sub(r'^\d+;url=','', fref, flags=re.IGNORECASE)
			if re.search(r'^\d+$', str(fref).strip()):
				if get_archive_url.tries < get_archive_url.max_tries:
					get_archive_url.tries += 1
					stime = min([8, int(fref)])
					sleep(stime)
					archive_url = get_archive_url(service_url, url, submitid)
				else:
					archive_url = ''
		elif floc:
			archive_url = floc

		if re.search(r'^(https?:)?//archive\.(fo|is|today|li|vn|md)/submit/?',archive_url):
			archive_url = ''
			# log("url: " + f.url + "\nloc: " + str(floc) + "\nref: " + str(fref) + "\ntext: " + str(freq.text), file="failed.txt")
		if re.search(r'^/cdn-cgi/', archive_url) and not re.search(r'/cdn-cgi/', service_url):
			archive_url = get_archive_url("https://archive.ph"+str(archive_url), f.url, submitid);

		# anything else is a failure

	get_archive_url.tries = 0
	return archive_url


get_archive_url.max_tries = 10
get_archive_url.tries = 0

main()
